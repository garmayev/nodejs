var http = require("http");

var message = "Hello World!";
http.createServer(function(request,response){
	console.log(response);
	response.end(message);
}).listen("3000", function (){
	console.log("Сервер начал прослушивание запросов");
});